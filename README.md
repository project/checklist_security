# Security Checklist


## Contents of This File

- [Description and Benefits](#description-and-benefits)
- [Installation and Usage](#installation-and-usage)
- [More Information](#more-information)


## Description and Benefits

The Checklist: Security module provides a list of steps that you can
take to increase the level of security on your Drupal site. It provides little functionality itself 
but helps you keep track of what needs to be done and what has been completed already.




## Installation and Usage

Checklist API is installed in the usual way. See [Installing contributed
modules](https://www.drupal.org/documentation/install/modules-themes/modules-8).
To start using the module, go to `/admin/config/search/security-checklist`. It will
pre-check any completed items it is able to auto detect. Start working through
the rest, and click "Save" to save your progress!


## More Information
- Enjoy using the module!
