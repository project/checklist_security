{#
/**
 * @file
 * Default theme implementation for the Security Checklist "Getting started" tab.
 *
 * Available variables:
 * - ga_query_string: A Google Analytics query string.
 *
 * @ingroup themeable
 * 
 * THE NAME OF THIS FILE MUST USE DASHES!!!!
 */
#}

<h3>Getting started with the Drupal Security Improvement Checklist</h3>
<p>Please read these instructions to get the most out of your security-configuration efforts.</p>

<h4>Purpose</h4>
<p>Many websites are designed by web developers with little to no security background. In today's world, lack of security best-practices knowledge is no longer acceptable. The Internet is over 30 years old, we have passed its innocent days of being solely a loving community of contributors and now face the burden of protecting both ourselves and our clients from attackers who seek valuable information and compensation for the data we create. As of 2020, attackers have already distrupted and held hostage: utilities such as power grids and water facilities, schools and businesses, and they have breached governmental security agencies. There are no longer any individuals, groups, or sectors some attacker is not willing to target. <strong>This checklist has been created to give web developers without a security background a foundation from which to build upon.</strong></p>

<p style="color: darkred">DO NOT consider this a complete list and do not consider it a substitution for an education in security. Reaching 100% on this checklist is not enough and you would serve both yourself and your clients well by pursuing additional education and courses on security.</p>

<h4>Recommended additional educational resources</h4>
<p>The developers of this checklist recommend looking into Security+, Networking+, and CISSP (internationally recognized) certifications for those interested. Other security courses are also encouraged.</p>

<p> Additionally, if you're building a U.S. government website, then your site will be evaulated in the <abbr title="Evaluates whether a software or service can even be tested in a secure environment">Authority To Test</abbr> and <abbr title="Evaluates whether a software or service can even be made publicly available">Authority To Operate</abbr> processes against the <a href="https://nvd.nist.gov/800-53">NIST 800-53 security controls</a>. You will also want to check out Digital.gov's <a title="The relevant laws, policies, and regulations for federal agencies" href="https://digital.gov/resources/checklist-of-requirements-for-federal-digital-services/?dg">Checklist of Requirements for Federal Websites and Digital Services</a> and their checklist for <a title="A list of required links that all federal websites need to have" href="https://digital.gov/resources/required-web-content-and-links/?dg">Required Web Content and Links</a>.</p>

<h4>Getting started</h4>
<p>Each time you open the Security Checklist, it will look to see if any tasks have already been completed. For example, if you've already turned on the Honeypot module then that item will be checked. You still need to click "Save" to time and date stamp the automatically-checked items.</p>
<p>The best way to proceed is to start at the top and work your way through each tab until you're done, clicking save after each completed item.</p>

<h4>How it's organized</h4>
<p>The sections group similar security modules based on their purpose/functionality. Each area is important, and evaluating each module is something you need to do to decide if its features are recommended for your site.</p>
<ul>
  <li><strong>Foundations:</strong> 
  Items in this section should be easy wins. They help developers manage a website more easily.</li>

  <li><strong>Audit &amp; Logging:</strong> 
  Being able to track down bad actors begins with an audit trail and logs. Enable now so you have the trail you need later.</li>

  <li><strong>Protection:</strong> 
  This section's goal is to add preventative measures that stop bad actors quickly or that make it harder for them to access your data.</li>

  <li><strong>Control:</strong> 
  Roles and permissions are essential for a secure site, but developers can go further with content controls and permissions limitations.</li>

  <li><strong>User Authentication:</strong> 
  These security modules directly relate to security when users authenticate with the site.</li>

  <li><strong>Governmental Requirements:</strong> 
  These modules provide functionality recommended by digital.gov.</li>

  <li><strong>Adjascent:</strong> 
  These modules provide services that contribute functionality that may not be directly security related, but fill in gaps that are otherwise missed.</li>

  <li><strong>Beyond:</strong> 
  This section discusses best practices that should be performed outside of the website itself.</li>
  
</ul>
<h5>Important elements:</h5>
<ul>
  <li><strong>Save Button:</strong> Be sure to click the save button after you check off each item. This will create a time and date stamp so that you can easily see when each task was completed.</li>
  <li><strong>Links:</strong> Many tasks have links next to them. Some links are to drupal.org, outside websites, or to admin sections of your own site. Links to outside resources will open in a new window.</li>
  <li><strong>Help:</strong> Some items have "More info" links. These will take you to appropriate documentation pages where you can read more about a module or important concept.</li>
</ul>

<h4>A note about pre-release modules</h4>
<p><em>Some recommended modules may not be considered ready for production websites. These modules are usually marked with "release-candidate" or "beta" or "dev" or "alpha" on their Drupal.org project page. Please be very careful when installing any module that doesn't have a full release version and has a green shield on its available version.</em></p>
<p>Additionally, some modules may not be covered under Drupal's Security Team review. Be wary of these modules and make sure you get the approval of your website's stakeholders before installing them, even if the module is on this list. Modules that aren't reviewed by Drupal's security team may have unpatched vulnerabilities that open your site up to attack.</p>

<h4>This checklist is incomplete</h4>
<p>If there is a module that isn't on this list and you feel should be added to it, please file a request in the issue queue. The Drupal community's module list is vast and not everyone can know everything.</p>

<h3>Disclaimers</h3>
<ul>
<li>This checklist is not complete. The community will always be creating new modules and the Drupal security team will always be updating Drupal to meet the threats on the internet, if you would like to add a recommendation to this list, please contact the maintainers.</li>
<li>This checklist doesn't actually provide any security features itself, it just makes suggestions your site can choose to implement or not.</li>
<li>The modules (and their creators) referenced in this list have no connections to this checklist's maintainers. No module is on this list for any reason other than it serving some function related to website security.</li>
<li>Most Importantly: YOU, your development team, and/or your security team are responsible for the security of your site. Following the guidelines of this checklist is no guarantee that your site will be protected against any given attack.</li>
</ul>

<h4>Credits</h4>

<p>The Drupal Security Checklist was created by <a href="https://www.drupal.org/user/1808716">Rex Barkdoll</a> based (directly) on the Drupal SEO Checklist module by <a href="https://www.drupal.org/user/46676">Ben Finklea</a>, CEO of <a href="http://www.volacci.com/utm_source=security_checklist&utm_medium=backend&utm_content=logo&utm_campaign=volacci_seo">Volacci</a> and a long-time Drupal community member. Development was paid for exclusively by PAE Inc and the National Institute of Corrections.</p>

<p>Special thanks to <a href="https://www.drupal.org/u/TravisCarden">Travis Carden</a> who created the <a href="https://www.drupal.org/project/checklistapi">Checklist API module</a>.</p>
